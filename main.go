package main

import (
	"flag"
	"log"
	"net/http"

	"git.lubar.me/ben/tolpaha/db"
	"git.lubar.me/ben/tolpaha/web"
)

func main() {
	flagHTTP := flag.String("http", ":8000", "[host]:port to listen for HTTP connections on")
	flagDB := flag.String("db", "user=tolpaha dbname=tolpaha sslmode=disable", "PostgreSQL database connection string")

	flag.Parse()

	if err := db.Init(*flagDB); err != nil {
		log.Fatalf("error initializing database: %+v", err)
	}

	log.Println("listening for requests on " + *flagHTTP)
	if err := http.ListenAndServe(*flagHTTP, web.Router); err != nil {
		log.Fatalf("error starting web server: %+v", err)
	}
}
