package db

import (
	"database/sql"
	"encoding/hex"
	"log"

	"github.com/gorilla/securecookie"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

const latestVersion = 3

var db *sql.DB
var SecureCookie *securecookie.SecureCookie

func Init(dsn string) error {
	var err error
	db, err = sql.Open("postgres", dsn)
	if err != nil {
		return errors.Wrap(err, "opening db")
	}

	tx, err := db.Begin()
	if err != nil {
		return errors.Wrap(err, "starting db schema check")
	}
	defer tx.Rollback()

	version := -1

	var settingsExists bool
	if err := tx.QueryRow(`SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE table_name = 'global_settings')`).Scan(&settingsExists); err != nil {
		return errors.Wrap(err, "checking existence of global_settings table")
	}

	if settingsExists {
		if err := tx.QueryRow(`SELECT "value" FROM "global_settings" WHERE "key" = 'schema_version'`).Scan(&version); err != nil {
			return errors.Wrap(err, "checking schema version")
		}
	}

	if err := updateSchema(tx, version); err != nil {
		return errors.Wrap(err, "updating database schema")
	}

	return errors.Wrap(tx.Commit(), "committing db schema check")
}

func updateSchema(tx *sql.Tx, version int) (err error) {
	type throw struct{ err error }
	defer func() {
		if r := recover(); r != nil {
			err = r.(throw).err
		}
	}()
	changed := false
	exec := func(query string, args ...interface{}) {
		_, err := tx.Exec(query, args...)
		if err != nil {
			panic(throw{
				errors.Wrap(err, "updating database schema"),
			})
		}
	}
	migrate := func(oldVersion int, name string, f func()) {
		if version != oldVersion {
			return
		}

		changed = true
		version = oldVersion + 1

		log.Printf("running schema migration for version %d: %q", version, name)
		f()
	}

	migrate(-1, "initialize empty database", func() {
		exec(`CREATE TABLE "global_settings" (
	"key" TEXT NOT NULL PRIMARY KEY,
	"value" TEXT NOT NULL
)`)
		exec(`INSERT INTO "global_settings" ("key", "value") VALUES ('schema_version', '-1')`)
	})

	migrate(0, "create categories table", func() {
		exec(`CREATE TABLE "categories" (
	"id" BIGSERIAL NOT NULL PRIMARY KEY,
	"name" TEXT NOT NULL,
	"description" TEXT NOT NULL DEFAULT '',
	"parent_id" BIGINT DEFAULT NULL REFERENCES "categories" ("id"),
	"layout_only" BOOLEAN NOT NULL DEFAULT FALSE,
	"relative_order" INT NOT NULL DEFAULT 0
)`)
		exec(`CREATE INDEX "categories_order" ON "categories" ("parent_id", "relative_order")`)
	})

	migrate(1, "add site title and tagline", func() {
		exec(`INSERT INTO "global_settings" ("key", "value") VALUES ($1, $2)`, "site_title", "Untitled Forum")
		exec(`INSERT INTO "global_settings" ("key", "value") VALUES ($1, $2)`, "site_tagline", "This text can be changed in the admin panel.")
	})

	migrate(2, "add cookie signing/encryption keys", func() {
		cookieKey := securecookie.GenerateRandomKey(64 + 16)

		exec(`INSERT INTO "global_settings" ("key", "value") VALUES ($1, $2)`, "cookie_secret_key", hex.EncodeToString(cookieKey))
	})

	var cookieKeyString string
	if err := tx.QueryRow(`SELECT "value" FROM "global_settings" WHERE "key" = 'cookie_secret_key'`).Scan(&cookieKeyString); err != nil && err != sql.ErrNoRows {
		return errors.Wrap(err, "retrieving the session encryption key from the database")
	}

	cookieKey, err := hex.DecodeString(cookieKeyString)
	if err != nil || len(cookieKey) != 64+16 {
		log.Println("WARNING: setting new value for cookie_secret_key. existing value was invalid. all sessions will be reset")
		cookieKey = securecookie.GenerateRandomKey(64 + 16)
		exec(`INSERT INTO "global_settings" ("key", "value") VALUES ('cookie_secret_key', $1) ON CONFLICT DO UPDATE SET "value" = $1`, hex.EncodeToString(cookieKey))
	}

	SecureCookie = securecookie.New(cookieKey[:64], cookieKey[64:])

	if version != latestVersion {
		return errors.Errorf("db: unknown database schema version %d (latest is %d)", version, latestVersion)
	}

	if changed {
		exec(`UPDATE "global_settings" SET "value" = $1 WHERE "key" = 'schema_version'`, version)
	}

	return
}
