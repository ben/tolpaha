package db

import (
	"database/sql"

	"github.com/avelino/slugify"
	"github.com/pkg/errors"
)

type Category struct {
	ID          ID
	Name        string
	Description string
	LayoutOnly  bool
	ParentID    ID

	Children []*Category
}

func (tx *Tx) readCategories(rows *sql.Rows, byID map[ID]*Category) []*Category {
	var categories []*Category

	for rows.Next() {
		var (
			id            ID
			parentID      sql.NullString
			name          string
			description   string
			layoutOnly    bool
			relativeOrder int
			depth         int
		)

		if err := rows.Scan(&id, &parentID, &name, &description, &layoutOnly, &relativeOrder, &depth); err != nil {
			tx.err = errors.Wrap(err, "scanning categories")
			return nil
		}

		c := &Category{
			ID:          id,
			ParentID:    parentID.String,
			Name:        name,
			Description: description,
			LayoutOnly:  layoutOnly,
		}

		if parentID.Valid {
			parent := byID[parentID.String]
			parent.Children = append(parent.Children, c)
		} else {
			categories = append(categories, c)
		}

		byID[id] = c
	}

	if err := rows.Err(); err != nil {
		tx.err = errors.Wrap(err, "scanning categories")
		return nil
	}

	return categories
}

func (tx *Tx) Category(id ID) *Category {
	if tx.err != nil {
		return nil
	}

	rows, err := tx.tx.Query(`
SELECT c0."id", c0."parent_id", c0."name", c0."description", c0."layout_only", c0."relative_order", 0 "depth"
  FROM "categories" c0
 WHERE c0."id" = $1

UNION ALL

SELECT c1."id", c1."parent_id", c1."name", c1."description", c1."layout_only", c1."relative_order", 1 "depth"
  FROM "categories" c1
 WHERE c1."parent_id" = $1

UNION ALL

SELECT c2."id", c2."parent_id", c2."name", c2."description", c2."layout_only", c2."relative_order", 2 "depth"
  FROM "categories" c2
 INNER JOIN "categories" c2p
    ON c2."parent_id" = c2p."id"
 WHERE c2p."parent_id" = $1
   AND NOT c2."layout_only"

 ORDER BY "depth" ASC, "parent_id" ASC, "relative_order" ASC
`, id)
	if err != nil {
		tx.err = errors.Wrapf(err, "requesting category id %q", id)
		return nil
	}

	defer rows.Close()

	var c Category
	if !rows.Next() {
		return nil
	}

	var (
		parentID      sql.NullString
		relativeOrder int
		depth         int
	)

	if err := rows.Scan(&c.ID, &parentID, &c.Name, &c.Description, &c.LayoutOnly, &relativeOrder, &depth); err != nil {
		tx.err = errors.Wrap(err, "scanning categories")
		return nil
	}
	c.ParentID = parentID.String

	tx.readCategories(rows, map[ID]*Category{
		c.ID: &c,
	})

	return &c
}

func (tx *Tx) CategorySlim(id ID) *Category {
	if tx.err != nil {
		return nil
	}

	var c Category
	var parentID sql.NullString
	if err := tx.tx.QueryRow(`
SELECT "id", "parent_id", "name","description", "layout_only"
  FROM "categories"
 WHERE "id" = $1
`, id).Scan(&c.ID, &parentID, &c.Name, &c.Description, &c.LayoutOnly); err != nil {
		if err != sql.ErrNoRows {
			tx.err = errors.Wrapf(err, "requesting category id %q", id)
		}
		return nil
	}
	c.ParentID = parentID.String

	return &c
}

func (tx *Tx) TopLevelCategories() []*Category {
	if tx.err != nil {
		return nil
	}

	rows, err := tx.tx.Query(`
SELECT c0."id", c0."parent_id", c0."name", c0."description", c0."layout_only", c0."relative_order", 0 "depth"
  FROM "categories" c0
 WHERE c0."parent_id" IS NULL

UNION ALL

SELECT c1."id", c1."parent_id", c1."name", c1."description", c1."layout_only", c1."relative_order", 1 "depth"
  FROM "categories" c1
 INNER JOIN "categories" c1p
    ON c1."parent_id" = c1p."id"
 WHERE c1p."parent_id" IS NULL

UNION ALL

SELECT c2."id", c2."parent_id", c2."name", c2."description", c2."layout_only", c2."relative_order", 2 "depth"
  FROM "categories" c2
 INNER JOIN "categories" c2p
    ON c2."parent_id" = c2p."id"
 INNER JOIN "categories" c2pp
    ON c2p."parent_id" = c2pp."id"
 WHERE c2pp."parent_id" IS NULL
   AND NOT c2."layout_only"

 ORDER BY "depth" ASC, "parent_id" ASC, "relative_order" ASC
`)
	if err != nil {
		tx.err = errors.Wrap(err, "requesting top level categories")
		return nil
	}

	defer rows.Close()

	return tx.readCategories(rows, make(map[ID]*Category))
}

func (c *Category) Slug() string {
	return slugify.Slugify(c.Name)
}
