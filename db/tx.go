package db

import (
	"context"
	"database/sql"

	"github.com/pkg/errors"
)

// ID is an opaque string.
type ID = string

var ReadOnly = &sql.TxOptions{ReadOnly: true}

type Tx struct {
	tx  *sql.Tx
	err error
}

func Begin(ctx context.Context, opts *sql.TxOptions) *Tx {
	tx, err := db.BeginTx(ctx, opts)
	return &Tx{
		tx:  tx,
		err: errors.Wrap(err, "starting db transaction"),
	}
}

func (tx *Tx) Cleanup() {
	if tx.tx != nil {
		_ = tx.tx.Rollback()
	}
}

func (tx *Tx) Done() error {
	if tx.err == nil {
		tx.err = errors.Wrap(tx.tx.Commit(), "committing transaction")
	}

	return tx.err
}

func (tx *Tx) GlobalSetting(key string) string {
	if tx.err != nil {
		return ""
	}

	var value string
	if err := tx.tx.QueryRow(`SELECT "value" FROM "global_settings" WHERE "key" = $1`, key).Scan(&value); err != nil {
		if err != sql.ErrNoRows {
			tx.err = errors.Wrapf(err, "querying global setting %q", key)
		}
		return ""
	}

	return value
}
