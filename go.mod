module git.lubar.me/ben/tolpaha

go 1.13

require (
	github.com/avelino/slugify v0.0.0-20180501145920-855f152bd774
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/securecookie v1.1.1
	github.com/lib/pq v1.6.0
	github.com/pkg/errors v0.9.1
)
