package web

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/base64"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"

	"git.lubar.me/ben/tolpaha/assets"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

const mdash = " \u2014 "

var tmpl = func() *template.Template {
	t := template.New("").Funcs(template.FuncMap{
		"ctx": func() context.Context {
			panic("ctx template func should have been overridden")
		},
		"integrity": staticIntegrity,
		"route": func(string) *mux.Route {
			panic("route template func should have been overridden")
		},
		"csrfToken": csrfToken,
	})

	for _, name := range assets.AssetNames() {
		if !strings.HasPrefix(name, "templates/") || !strings.HasSuffix(name, ".tmpl") {
			continue
		}

		template.Must(t.New(strings.TrimSuffix(strings.TrimPrefix(name, "templates/"), ".tmpl")).Parse(string(assets.MustAsset(name))))
	}

	return t
}()

func init() {
	// deferred to avoid initialization loop
	tmpl.Funcs(template.FuncMap{"route": Router.Get})
}

var (
	tmplNotFound      = mustTmpl("not-found")
	tmplHome          = mustTmpl("home")
	tmplInternalError = mustTmpl("internal-error")
	tmplCategory      = mustTmpl("category")
	tmplLogin         = mustTmpl("login")
	styleIntegrity, _ = staticIntegrity("static/style.css")
)

func mustTmpl(name string) *template.Template {
	t := tmpl.Lookup(name)
	if t == nil {
		panic("missing template: " + name)
	}
	return t
}

func ServeTemplate(w http.ResponseWriter, r *http.Request, status int, t *template.Template, data interface{}) {
	var buf bytes.Buffer
	t, err := t.Clone()
	if err == nil {
		err = t.Funcs(template.FuncMap{
			"ctx": r.Context,
		}).Execute(&buf, data)
	}

	if err != nil {
		if t == tmplInternalError {
			// avoid recursion
			log.Panicf("template error in error handler: %+v", err)
			return
		}

		InternalError(w, r, "template", errors.WithStack(err))
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	w.Header().Set("Cache-Control", "private")
	w.Header().Add("Link", "</static/style.css>; rel=preload; as=style; integrity=\""+styleIntegrity+"\"")
	w.WriteHeader(status)
	_, _ = buf.WriteTo(w) // errors are probably out of our control
}

type Shared struct {
	Title     string
	BodyClass string
}

func staticIntegrity(name string) (string, error) {
	b, err := assets.Asset(name)
	if err != nil {
		return "", err
	}

	sum := sha256.Sum256(b)
	return "sha256-" + base64.StdEncoding.EncodeToString(sum[:]), nil
}
