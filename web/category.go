package web

import (
	"net/http"

	"git.lubar.me/ben/tolpaha/db"
	"github.com/gorilla/mux"
)

func serveCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	tx := db.Begin(r.Context(), db.ReadOnly)
	defer tx.Cleanup()

	cat := tx.Category(vars["id"])

	var parent *db.Category
	if cat != nil && cat.ParentID != "" {
		parent = tx.CategorySlim(cat.ParentID)
	}

	if err := tx.Done(); err != nil {
		InternalError(w, r, "database", err)
		return
	}

	if cat == nil {
		NotFound(w, r)
		return
	}

	if vars["slug"] != cat.Slug() {
		u, err := Router.Get("category").URL("id", cat.ID, "slug", cat.Slug())
		if err != nil {
			InternalError(w, r, "redirect", err)
			return
		}
		http.Redirect(w, r, u.String(), http.StatusMovedPermanently)
		return
	}

	title := cat.Name
	if cat.Description != "" {
		title += mdash + cat.Description
	}
	ServeTemplate(w, r, http.StatusOK, tmplCategory, &categoryData{
		Shared: Shared{
			Title: title,
		},
		Parent:   parent,
		Category: cat,
	})
}

type categoryData struct {
	Shared      Shared
	Parent      *db.Category
	Category    *db.Category
	Discussions []*struct{}
}
