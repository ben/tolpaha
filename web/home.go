package web

import (
	"net/http"

	"git.lubar.me/ben/tolpaha/db"
)

func serveHome(w http.ResponseWriter, r *http.Request) {
	tx := db.Begin(r.Context(), db.ReadOnly)
	defer tx.Cleanup()

	title := tx.GlobalSetting("site_title")
	tagline := tx.GlobalSetting("site_tagline")

	categories := tx.TopLevelCategories()

	if err := tx.Done(); err != nil {
		InternalError(w, r, "database", err)
		return
	}

	pageTitle := title
	if tagline != "" {
		pageTitle += mdash + tagline
	}

	ServeTemplate(w, r, http.StatusOK, tmplHome, &homeData{
		Shared: Shared{
			Title: pageTitle,
		},
		Title:   title,
		Tagline: tagline,

		Categories: categories,
	})
}

type homeData struct {
	Shared     Shared
	Title      string
	Tagline    string
	Categories []*db.Category
}
