package web

import (
	"net/http"
	"strings"

	"git.lubar.me/ben/tolpaha/assets"
	"github.com/gorilla/mux"
)

var Router = mux.NewRouter()

func init() {
	Router.NotFoundHandler = http.HandlerFunc(NotFound)

	assetHandler := http.FileServer(assets.AssetFile())
	for _, name := range assets.AssetNames() {
		if strings.HasPrefix(name, "static/") {
			Router.Handle("/"+name, assetHandler).Methods("GET")
		}
	}

	Router.Use(checkSessionCookie)

	Router.HandleFunc("/", serveHome).Methods("GET").Name("home")
	Router.HandleFunc("/c/{id:[0-9]+}-{slug}", serveCategory).Methods("GET").Name("category")
	Router.HandleFunc("/login", serveLogin).Methods("GET", "POST").Name("log-in")
}
