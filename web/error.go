package web

import (
	"log"
	"net/http"

	"github.com/google/uuid"
)

func init() {
	Router.NotFoundHandler = http.HandlerFunc(NotFound)
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	ServeTemplate(w, r, http.StatusNotFound, tmplNotFound, &notFoundData{
		Shared: Shared{
			Title:     "Not Found",
			BodyClass: "single-column",
		},
		RequestURI: r.URL.RequestURI(),
	})
}

type notFoundData struct {
	Shared     Shared
	RequestURI string
}

func InternalError(w http.ResponseWriter, r *http.Request, category string, err error) {
	id := uuid.New()

	log.Printf("ERROR[%s:%v] in %s %q: %+v", category, id, r.Method, r.URL.RequestURI(), err)

	ServeTemplate(w, r, http.StatusInternalServerError, tmplInternalError, &internalErrorData{
		Shared: Shared{
			Title:     "Internal Error",
			BodyClass: "single-column",
		},
		Category: category,
		ID:       id,
	})
}

type internalErrorData struct {
	Shared   Shared
	Category string
	ID       uuid.UUID
}
