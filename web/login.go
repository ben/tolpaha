package web

import "net/http"

func serveLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		if !CheckCSRFToken(r.Context(), r.FormValue("csrf_token")) {
			http.Redirect(w, r, "/login?error=invalid_csrf", http.StatusFound)
			return
		}

		http.Redirect(w, r, "/login?error=invalid_user", http.StatusFound)
		return
	}

	r = EnsureCSRFKey(w, r)

	ServeTemplate(w, r, http.StatusOK, tmplLogin, &loginData{
		Shared: Shared{
			Title:     "Log in",
			BodyClass: "single-column",
		},
		Error: r.FormValue("error"),
	})
}

type loginData struct {
	Shared Shared
	Error  string
}
