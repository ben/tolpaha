package web

import (
	"context"
	"crypto/subtle"
	"encoding/base64"
	"errors"
	"net/http"
	"time"

	"git.lubar.me/ben/tolpaha/db"
	"github.com/gorilla/securecookie"
)

type sessionContextKey struct{}
type csrfKeySet struct{}
type SessionData struct {
	ResetAfter time.Time
	UserID     db.ID
	CSRFToken  [16]byte
}

func checkSessionCookie(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c, err := r.Cookie("tolpaha_session")
		if err != nil {
			next.ServeHTTP(w, r)
			return
		}

		var sessionData SessionData

		if err = db.SecureCookie.Decode(c.Name, c.Value, &sessionData); err != nil {
			// clear invalid cookie
			_ = SetSessionCookie(w, nil)

			next.ServeHTTP(w, r)
			return
		}

		if sessionData.UserID != "" && time.Now().After(sessionData.ResetAfter) {
			// update session cookie expiry, but only if the user is logged in
			_ = SetSessionCookie(w, &sessionData)
		}
		r = r.WithContext(context.WithValue(r.Context(), sessionContextKey{}, &sessionData))

		next.ServeHTTP(w, r)
	})
}

func GetSessionData(ctx context.Context) *SessionData {
	sd, _ := ctx.Value(sessionContextKey{}).(*SessionData)
	return sd
}

func SetSessionCookie(w http.ResponseWriter, sd *SessionData) error {
	c := &http.Cookie{
		Name:     "tolpaha_session",
		Path:     "/",
		SameSite: http.SameSiteStrictMode,
		HttpOnly: true,
		Secure:   true,
	}

	if sd != nil {
		sd.ResetAfter = time.Now().Add(14 * 24 * time.Hour)
		c.Expires = time.Now().Add(30 * 24 * time.Hour)

		var err error
		c.Value, err = db.SecureCookie.Encode(c.Name, sd)
		if err != nil {
			return err
		}
	}

	http.SetCookie(w, c)

	return nil
}

func EnsureCSRFKey(w http.ResponseWriter, r *http.Request) *http.Request {
	sd := GetSessionData(r.Context())
	if sd == nil {
		sd = &SessionData{}
	}

	if sd.CSRFToken == [16]byte{} {
		copy(sd.CSRFToken[:], securecookie.GenerateRandomKey(16))
		SetSessionCookie(w, sd)
		r = r.WithContext(context.WithValue(r.Context(), sessionContextKey{}, sd))
	}

	r = r.WithContext(context.WithValue(r.Context(), csrfKeySet{}, true))

	return r
}

func csrfToken(ctx context.Context) (string, error) {
	sd := GetSessionData(ctx)
	if sd == nil {
		return "", errors.New("csrfToken: no session is active")
	}

	if ctx.Value(csrfKeySet{}) != true {
		return "", errors.New("csrfToken: EnsureCSRFKey was not called")
	}

	var buf [32]byte
	copy(buf[:], securecookie.GenerateRandomKey(16))
	for i, j := 0, 16; i < 16; i, j = i+1, j+1 {
		buf[j] = buf[i] ^ sd.CSRFToken[i]
	}

	return base64.RawURLEncoding.EncodeToString(buf[:]), nil
}

func CheckCSRFToken(ctx context.Context, key string) bool {
	data, err := base64.RawURLEncoding.DecodeString(key)
	if err != nil || len(data) != 32 {
		return false
	}

	sd := GetSessionData(ctx)
	if sd == nil {
		// no session
		return false
	}

	assignedKey := subtle.ConstantTimeCompare(sd.CSRFToken[:], make([]byte, 16)) ^ 1

	for i, j := 0, 16; i < 16; i, j = i+1, j+1 {
		data[j] = data[i] ^ data[j]
	}

	matchingKey := subtle.ConstantTimeCompare(sd.CSRFToken[:], data[16:])

	return (assignedKey & matchingKey) == 1
}
